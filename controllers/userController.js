const _ = require("lodash");
const bcrypt = require('bcrypt');

const { User, validate } = require('../models/userModel');


module.exports = {
    createUser: async (req, res) => {

        const { first_name, last_name, email, phone_number, address, password, confirm_password } = req.body

        let user = await User.findOne({ email });
        if (user) return res.status(400).send({ message: "User already registered." });

        if (password !== confirm_password) return res.status(400).send({ message: "Password confirmation doesn't match" });

        user = new User(
            _.pick(req.body, ["first_name", "last_name", "email", "phone_number", 'address', "password",])
        );

        const salt = await bcrypt.genSalt(10);
        user.password = await bcrypt.hash(user.password, salt);
        await user.save();

        const token = user.generateAuthToken();
        res.header('x-auth-token', token).send(_.pick(user, ['_id', 'first_name', 'last_name', 'email', 'phone_number', 'address']));
    },

    signIn: async (req, res) => {
        let user = await User.findOne({ email: req.body.email });
        if (!user) return res.status(400).send({ message: "Invalid email or password!" });


        const validPassword = await bcrypt.compare(req.body.password, user.password);
        if (!validPassword) return res.status(400).send("Invalid email or password.");

        const token = user.generateAuthToken();
        res.send({ token });
    }

}