const mongoose = require("mongoose");
const express = require("express");
const Joi = require("joi");

const validate = require("../middleware/validate");

const router = express.Router();

const auth = require("../middleware/auth");
const controller = require('../controllers/transactionController');



router.post('/fundAccount/:id', auth, controller.fundAccount)
router.post('/withdrawfunds/:id', auth, controller.withdrawFunds)
router.post('/transferFunds/:id', auth, controller.transferFunds)

// function validateAuth(req) {
//     const schema = Joi.object({
//         amount: Joi.number().required().precision(3),
//         transactionType: Joi.string().required(),
//         balance: Joi.number().precision(3).required(),
//     });

//     return schema.validate(req);
// }

module.exports = router;